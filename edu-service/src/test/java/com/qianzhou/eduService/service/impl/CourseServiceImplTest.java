package com.qianzhou.eduService.service.impl;

import com.qianzhou.bo.CourseBO;
import com.qianzhou.dao.CourseDao;
import com.qianzhou.vo.CourseVO;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

/**
 * 描述：CourseServiceImplTest测试类
 */
@SpringBootTest
class CourseServiceImplTest {
    @Autowired
    private CourseDao courseDao;
    @Test
    void getAllCourse() {
        List<CourseBO> allCourse = courseDao.getAllCourse();
        for (CourseBO bo: allCourse
             ) {
            System.out.println(bo);
        }
        List<CourseVO> courseVOS = CourseBO.boListConvertVoList(allCourse);
        for (CourseVO vo: courseVOS
        ) {
            System.out.println(vo);
        }
    }

    @Test
    void getCourseByUserId() {
        List<CourseBO> courseBOS = courseDao.getCourseByUserId(100030018);
        for (CourseBO bo: courseBOS
        ) {
            System.out.println(bo);
        }
    }
}