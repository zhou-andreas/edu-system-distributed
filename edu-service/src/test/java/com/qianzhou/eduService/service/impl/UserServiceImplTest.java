package com.qianzhou.eduService.service.impl;

import com.qianzhou.dao.UserDao;
import com.qianzhou.entity.User;
import org.junit.jupiter.api.Test;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * 描述：UserService测试类
 */
@SpringBootTest
class UserServiceImplTest {
    @Autowired
    private UserDao userDao;
    @Test
    void getUser() {
        User user = userDao.queryById(100030011);
        System.out.println(user);
    }
}