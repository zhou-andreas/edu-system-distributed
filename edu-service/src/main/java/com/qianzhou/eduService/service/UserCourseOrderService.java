package com.qianzhou.eduService.service;

import com.qianzhou.dto.UserCourseOrderDTO;
import com.qianzhou.entity.UserCourseOrder;
import com.qianzhou.vo.UserCourseOrderVO;

import java.util.List;

/**
 * 描述：OrderService接口
 */
public interface UserCourseOrderService {
    /**
     * 功能描述
     *
     * @param dto
     * @return void
     * @author qian.zhou
     * @date 2021/8/29
     */
    void saveOrder(UserCourseOrderDTO dto);

    /**
     * 功能描述 修改订单状态
     *
     * @param orderNo 订单号
     * @return void
     * @author qian.zhou
     * @date 2021/8/29
     */
    Integer updateOrder(Long orderNo, Byte status);

    /**
     * 功能描述 删除订单
     *
     * @param orderNo
     * @return void
     * @author qian.zhou
     * @date 2021/8/29
     */

    Integer deleteOrder(Long orderNo);

    /**
     * 功能描述 查询已登录用户的全部订单
     *
     * @param userId
     * @return java.util.List<com.qianzhou.vo.UserCourseOrderVO>
     * @author qian.zhou
     * @date 2021/8/29
     */

    List<UserCourseOrderVO> getOrdersByUserId(Integer userId);
}
