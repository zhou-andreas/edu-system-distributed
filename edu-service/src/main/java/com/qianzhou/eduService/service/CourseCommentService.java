package com.qianzhou.eduService.service;

import com.qianzhou.dto.CourseCommentDTO;
import com.qianzhou.entity.CourseComment;
import com.qianzhou.vo.CourseCommentVO;

import java.util.List;

/**
 * @Author: qian.zhou
 * @Description:
 * @Date Created in 2021-08-29-18:04
 * @Modified By:
 */
public interface CourseCommentService {
    /**
     * 保存留言
     * @param dto 留言内容对象
     * @return 受影响的行数
     */
    Integer saveCourseComment(CourseCommentDTO dto);

    /**
     * 某个课程的全部留言（分页）
     * @param courseId 课程编号
     * @param offset 数据偏移
     * @param pageSize 每页条目数
     * @return 留言集合
     */
    List<CourseCommentVO> getCommentsByCourseId(Integer courseId, Integer offset, Integer pageSize);

    /**
     * 点赞
     * @param commentId 留言编号
     * @param userId 用户编号
     * @return 0：保存失败，1：保存成功
     */
    Integer saveFavorite(Integer commentId,Integer userId);

    /**
     * 取消赞
     * @param commentId 留言编号
     * @param userid 用户编号
     * @return 0：保存失败，1：保存成功
     */
    Integer cancelFavorite(Integer commentId,Integer userid);
}
