package com.qianzhou.eduService.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.qianzhou.dao.CourseCommentDao;
import com.qianzhou.dto.CourseCommentDTO;
import com.qianzhou.eduService.service.CourseCommentService;
import com.qianzhou.entity.CourseComment;
import com.qianzhou.vo.CourseCommentVO;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * @Author: qian.zhou
 * @Description:
 * @Date Created in 2021-08-29-18:06
 * @Modified By:
 */
@Service(interfaceClass = CourseCommentService.class)
public class CourseCommentServiceImpl implements CourseCommentService {
    @Autowired
    private CourseCommentDao courseCommentDao;

    /**
     * 功能描述 保存留言
     *
     * @param dto
     * @return java.lang.Integer
     * @author qian.zhou
     * @date 2021/8/29
     */
    @Override
    public Integer saveCourseComment(CourseCommentDTO dto) {
        return courseCommentDao.saveCourseComment(dto);
    }

    /**
     * 功能描述 根据id查找评论功能
     *
     * @param courseId
     * @param offset
     * @param pageSize
     * @return java.util.List<com.qianzhou.vo.CourseCommentVO>
     * @author qian.zhou
     * @date 2021/8/29
     */

    @Override
    public List<CourseCommentVO> getCommentsByCourseId(Integer courseId, Integer offset, Integer pageSize) {
        List<CourseComment> courseComments = courseCommentDao.getCommentsByCourseId(courseId, offset, pageSize);
        return CourseComment.poListConvertVoList(courseComments);
    }

    /**
     * 功能描述 保存点赞和喜欢数量
     * 点赞：
     * 先查看当前用户对的这条留言是否点过赞，
     * 如果点过：修改is_del状态即可，取消赞
     * 如果没点过：保存一条点赞的信息
     * <p>
     * 最终，更新赞的数量
     *
     * @param commentId
     * @param userId
     * @return java.lang.Integer
     * @author qian.zhou
     * @date 2021/8/30
     */

    @Override
    public Integer saveFavorite(Integer commentId, Integer userId) {
        Integer i = courseCommentDao.existsFavorite(commentId, userId);
        Integer i1 = 0;
        Integer i2 = 0;
        if (i == 0) { //没点过赞
            i1 = courseCommentDao.saveCommentFavorite(commentId, userId);
        } else {
            i1 = courseCommentDao.updateFavoriteStatus(0, commentId, userId);
        }
        i2 = courseCommentDao.updateLikeCount(1, commentId);

        if (i1 == 0 || i2 == 0) {
            return 500; // 点赞失败！！！
            /*throw  new RuntimeException("点赞失败！");*/
        }
        return commentId; // 点赞成功！！！
    }

    /**
     * 功能描述 cancelFavorite 用户取消点赞
     *
     * @param commentId
     * @param userId
     * @return java.lang.Integer
     * @author qian.zhou
     * @date 2021/8/30
     */

    @Override
    public Integer cancelFavorite(Integer commentId, Integer userId) {
        Integer i1 = courseCommentDao.updateFavoriteStatus(1, commentId, userId);
        Integer i2 = courseCommentDao.updateLikeCount(-1, commentId);

        if (i1 == 0 || i2 == 0) {
            return 500; // 取消赞失败！！！
            /*throw new RuntimeException("取消赞失败！");*/
        }
        return i2;
    }
}
