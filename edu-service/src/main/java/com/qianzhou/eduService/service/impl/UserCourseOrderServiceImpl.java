package com.qianzhou.eduService.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.qianzhou.dao.PayOrderDao;
import com.qianzhou.dao.UserCourseOrderDao;
import com.qianzhou.dto.UserCourseOrderDTO;
import com.qianzhou.eduService.service.UserCourseOrderService;
import com.qianzhou.entity.UserCourseOrder;
import com.qianzhou.vo.UserCourseOrderVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 描述：OrderServiceImpl实现类
 */
@Service(interfaceClass = UserCourseOrderService.class)
@Component
public class UserCourseOrderServiceImpl implements UserCourseOrderService {
    @Autowired
    private UserCourseOrderDao userCourseOrderDao;
    @Autowired
    private PayOrderDao payOrderDao;


    /**
     * 功能描述 保存订单信息
     *
     * @param dto
     * @return void
     * @author qian.zhou
     * @date 2021/8/29
     */
    @Override
    public void saveOrder(UserCourseOrderDTO dto) {
        UserCourseOrder userCourseOrder = UserCourseOrderDTO.dtoConvertPo(dto);
        userCourseOrderDao.savaOrder(userCourseOrder);
    }

    /**
     * 功能描述 修改订单状态
     *
     * @param orderNo 订单号
     * @return void
     * @author qian.zhou
     * @date 2021/8/29
     */
    @Override
    public Integer updateOrder(Long orderNo, Byte status) {
        Integer integer = userCourseOrderDao.updateOrder(orderNo, status);
        return integer;
    }

    /**
     * 功能描述 删除订单
     *
     * @param orderNo
     * @return void
     * @author qian.zhou
     * @date 2021/8/29
     */
    @Override
    public Integer deleteOrder(Long orderNo) {
        Integer integer = userCourseOrderDao.deleteOrder(orderNo);
        return integer;
    }

    /**
     * 功能描述 查询已登录用户的全部订单
     *
     * @param userId
     * @return java.util.List<com.qianzhou.vo.UserCourseOrderVO>
     * @author qian.zhou
     * @date 2021/8/29
     */
    @Override
    public List<UserCourseOrderVO> getOrdersByUserId(Integer userId) {
        List<UserCourseOrder> userCourseOrders = userCourseOrderDao.getOrdersByUserId(userId);
        // 将对应的entity转换为VO, 并且返回VO
        return UserCourseOrder.poListConvertVoList(userCourseOrders);
    }
}
