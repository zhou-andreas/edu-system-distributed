package com.qianzhou.eduService.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.qianzhou.bo.CourseBO;
import com.qianzhou.dao.CourseDao;
import com.qianzhou.eduService.service.CourseService;
import com.qianzhou.vo.CourseVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 描述：CourseServiceImpl实现类
 */
@Service(interfaceClass = CourseService.class)
@Component
public class CourseServiceImpl implements CourseService {
    @Autowired
    private CourseDao courseDao;

    @Override
    public List<CourseVO> getAllCourse() {
        List<CourseBO> courseBOS = courseDao.getAllCourse();
        return CourseBO.boListConvertVoList(courseBOS);
    }

    @Override
    public List<CourseVO> getCourseByUserId(Integer id) {
        List<CourseBO> courseBOS = courseDao.getCourseByUserId(id);
        // 将courseBOS转换为courseVOS
        return CourseBO.boListConvertVoList(courseBOS);
    }

    @Override
    public CourseVO getCourseById(Integer courseId) {
        CourseBO courseBO = courseDao.getCourseById(courseId);
        return CourseBO.boConvertVo(courseBO);
    }
}
