package com.qianzhou.eduService.service;

import com.qianzhou.dto.UserDTO;
import com.qianzhou.entity.User;
import com.qianzhou.vo.UserVO;

/**
 * 描述：UserService
 */
public interface UserService {
    /**
     * 功能描述 接口仅仅是测试使用
     *
     * @param id
     * @return com.qianzhou.entity.User
     * @author qian.zhou
     * @date 2021/8/29
     */

    User getUser(Integer id);

    /**
     * 功能描述 用户登录接口
     *
     * @param phone
     * @param password
     * @return com.qianzhou.entity.User
     * @author qian.zhou
     * @date 2021/8/29
     */

    UserVO login(String phone, String password);

    /**
     * 功能描述 检查手机号是否注册过
     *
     * @param phone
     * @return java.lang.Integer
     * @author qian.zhou
     * @date 2021/8/29
     */

    Integer checkPhone(String phone);

    /**
     * 功能描述 用户注册
     *
     * @param dto
     * @return java.lang.Integer
     * @author qian.zhou
     * @date 2021/8/29
     */

    Integer register(UserDTO dto);
}
