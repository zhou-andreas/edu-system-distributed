package com.qianzhou.eduService.service;

import com.qianzhou.entity.Course;
import com.qianzhou.vo.CourseVO;

import java.util.List;

/**
 * 描述：CourseService接口
 */
public interface CourseService {
    /**
     * @Author: andreaszhou
     * @Description: 查找所有课程
     * @DateTime: 2021/8/29 9:13
     * @Params:
     * @Return: List<CourseBO>
     */
    List<CourseVO> getAllCourse();

    /**
     * @Author: andreaszhou
     * @Description: 查看已购课程
     * @DateTime: 2021/8/29 9:13
     * @Params:
     * @Return
     */
    List<CourseVO> getCourseByUserId(Integer id);

    /**
     * 功能描述 查询某门课程的详细信息
     *
     * @param courseId
     * @return
     * @author qian.zhou
     * @date 2021/8/29
     */
    CourseVO getCourseById(Integer courseId);
}
