package com.qianzhou.eduService.service.impl;


import com.alibaba.dubbo.config.annotation.Service;
import com.qianzhou.dao.UserDao;
import com.qianzhou.dto.UserDTO;
import com.qianzhou.eduService.service.UserService;
import com.qianzhou.entity.User;
import com.qianzhou.vo.UserVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


/**
 * 描述：UserServiceImpl实现类
 */
@Service(interfaceClass = UserService.class)
@Component
public class UserServiceImpl implements UserService {
    @Autowired
    private UserDao userDao;

    /**
     * 功能描述 接口仅仅是测试使用
     *
     * @param id
     * @return com.qianzhou.entity.User
     * @author qian.zhou
     * @date 2021/8/29
     */

    @Override
    public User getUser(Integer id) {
        System.out.println(userDao.queryById(id));
        return userDao.queryById(id);
    }

    /**
     * 功能描述 用户登录
     *
     * @param phone
     * @param password
     * @return com.qianzhou.entity.User
     * @author qian.zhou
     * @date 2021/8/29
     */

    @Override
    public UserVO login(String phone, String password) {
        User user = userDao.Login(phone, password);
        UserVO userVO;
        if (user == null) {
            return null;
        } else {
            userVO = User.poConvertToVo(user);
        }
        return userVO;
    }

    /**
     * 功能描述 检查手机号是否注册过
     *
     * @param phone
     * @return java.lang.Integer
     * @author qian.zhou
     * @date 2021/8/29
     */

    @Override
    public Integer checkPhone(String phone) {
        return userDao.checkPhone(phone);
    }

    /**
     * 功能描述 用户注册
     *
     * @param dto
     * @return java.lang.Integer
     * @author qian.zhou
     * @date 2021/8/29
     */

    @Override
    public Integer register(UserDTO dto) {
        User user = UserDTO.dtoConvertToPo(dto);
        return userDao.register(user);
    }
}
