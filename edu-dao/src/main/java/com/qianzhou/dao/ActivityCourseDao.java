package com.qianzhou.dao;

import com.qianzhou.entity.ActivityCourse;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 活动课程表(ActivityCourse)表数据库访问层
 *
 * @author qian.zhou
 * @since 2021-08-28 13:40:01
 */
@Mapper
public interface ActivityCourseDao {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    ActivityCourse queryById(Integer id);

    /**
     * 查询指定行数据
     *
     * @param offset 查询起始位置
     * @param limit  查询条数
     * @return 对象列表
     */
    List<ActivityCourse> queryAllByLimit(@Param("offset") int offset, @Param("limit") int limit);


    /**
     * 通过实体作为筛选条件查询
     *
     * @param activityCourse 实例对象
     * @return 对象列表
     */
    List<ActivityCourse> queryAll(ActivityCourse activityCourse);

    /**
     * 新增数据
     *
     * @param activityCourse 实例对象
     * @return 影响行数
     */
    int insert(ActivityCourse activityCourse);

    /**
     * 批量新增数据（MyBatis原生foreach方法）
     *
     * @param entities List<ActivityCourse> 实例对象列表
     * @return 影响行数
     */
    int insertBatch(@Param("entities") List<ActivityCourse> entities);

    /**
     * 批量新增或按主键更新数据（MyBatis原生foreach方法）
     *
     * @param entities List<ActivityCourse> 实例对象列表
     * @return 影响行数
     */
    int insertOrUpdateBatch(@Param("entities") List<ActivityCourse> entities);

    /**
     * 修改数据
     *
     * @param activityCourse 实例对象
     * @return 影响行数
     */
    int update(ActivityCourse activityCourse);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 影响行数
     */
    int deleteById(Integer id);

}

