package com.qianzhou.dao;

import com.qianzhou.entity.PayOrderRecord;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 支付订单状态日志表(PayOrderRecord)表数据库访问层
 *
 * @author qian.zhou
 * @since 2021-08-28 13:40:06
 */
@Mapper
public interface PayOrderRecordDao {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    PayOrderRecord queryById(Integer id);

    /**
     * 查询指定行数据
     *
     * @param offset 查询起始位置
     * @param limit  查询条数
     * @return 对象列表
     */
    List<PayOrderRecord> queryAllByLimit(@Param("offset") int offset, @Param("limit") int limit);


    /**
     * 通过实体作为筛选条件查询
     *
     * @param payOrderRecord 实例对象
     * @return 对象列表
     */
    List<PayOrderRecord> queryAll(PayOrderRecord payOrderRecord);

    /**
     * 新增数据
     *
     * @param payOrderRecord 实例对象
     * @return 影响行数
     */
    int insert(PayOrderRecord payOrderRecord);

    /**
     * 批量新增数据（MyBatis原生foreach方法）
     *
     * @param entities List<PayOrderRecord> 实例对象列表
     * @return 影响行数
     */
    int insertBatch(@Param("entities") List<PayOrderRecord> entities);

    /**
     * 批量新增或按主键更新数据（MyBatis原生foreach方法）
     *
     * @param entities List<PayOrderRecord> 实例对象列表
     * @return 影响行数
     */
    int insertOrUpdateBatch(@Param("entities") List<PayOrderRecord> entities);

    /**
     * 修改数据
     *
     * @param payOrderRecord 实例对象
     * @return 影响行数
     */
    int update(PayOrderRecord payOrderRecord);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 影响行数
     */
    int deleteById(Integer id);

}

