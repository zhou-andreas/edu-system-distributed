package com.qianzhou.dao;

import com.qianzhou.entity.CourseCommentFavoriteRecord;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 课程留言点赞表(CourseCommentFavoriteRecord)表数据库访问层
 *
 * @author qian.zhou
 * @since 2021-08-28 13:40:04
 */
@Mapper
public interface CourseCommentFavoriteRecordDao {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    CourseCommentFavoriteRecord queryById(Integer id);

    /**
     * 查询指定行数据
     *
     * @param offset 查询起始位置
     * @param limit  查询条数
     * @return 对象列表
     */
    List<CourseCommentFavoriteRecord> queryAllByLimit(@Param("offset") int offset, @Param("limit") int limit);


    /**
     * 通过实体作为筛选条件查询
     *
     * @param courseCommentFavoriteRecord 实例对象
     * @return 对象列表
     */
    List<CourseCommentFavoriteRecord> queryAll(CourseCommentFavoriteRecord courseCommentFavoriteRecord);

    /**
     * 新增数据
     *
     * @param courseCommentFavoriteRecord 实例对象
     * @return 影响行数
     */
    int insert(CourseCommentFavoriteRecord courseCommentFavoriteRecord);

    /**
     * 批量新增数据（MyBatis原生foreach方法）
     *
     * @param entities List<CourseCommentFavoriteRecord> 实例对象列表
     * @return 影响行数
     */
    int insertBatch(@Param("entities") List<CourseCommentFavoriteRecord> entities);

    /**
     * 批量新增或按主键更新数据（MyBatis原生foreach方法）
     *
     * @param entities List<CourseCommentFavoriteRecord> 实例对象列表
     * @return 影响行数
     */
    int insertOrUpdateBatch(@Param("entities") List<CourseCommentFavoriteRecord> entities);

    /**
     * 修改数据
     *
     * @param courseCommentFavoriteRecord 实例对象
     * @return 影响行数
     */
    int update(CourseCommentFavoriteRecord courseCommentFavoriteRecord);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 影响行数
     */
    int deleteById(Integer id);

}

