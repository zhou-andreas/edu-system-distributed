package com.qianzhou.dao;

import com.qianzhou.dto.CourseCommentDTO;
import com.qianzhou.entity.CourseComment;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 课程留言表(CourseComment)表数据库访问层
 *
 * @author qian.zhou
 * @since 2021-08-28 13:40:04
 */
@Mapper
public interface CourseCommentDao {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    CourseComment queryById(Object id);

    /**
     * 查询指定行数据
     *
     * @param offset 查询起始位置
     * @param limit  查询条数
     * @return 对象列表
     */
    List<CourseComment> queryAllByLimit(@Param("offset") int offset, @Param("limit") int limit);


    /**
     * 通过实体作为筛选条件查询
     *
     * @param courseComment 实例对象
     * @return 对象列表
     */
    List<CourseComment> queryAll(CourseComment courseComment);

    /**
     * 新增数据
     *
     * @param courseComment 实例对象
     * @return 影响行数
     */
    int insert(CourseComment courseComment);

    /**
     * 批量新增数据（MyBatis原生foreach方法）
     *
     * @param entities List<CourseComment> 实例对象列表
     * @return 影响行数
     */
    int insertBatch(@Param("entities") List<CourseComment> entities);

    /**
     * 批量新增或按主键更新数据（MyBatis原生foreach方法）
     *
     * @param entities List<CourseComment> 实例对象列表
     * @return 影响行数
     */
    int insertOrUpdateBatch(@Param("entities") List<CourseComment> entities);

    /**
     * 修改数据
     *
     * @param courseComment 实例对象
     * @return 影响行数
     */
    int update(CourseComment courseComment);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 影响行数
     */
    int deleteById(Object id);

    /**
     * 功能描述 保存留言
     *
     * @param dto
     * @return java.lang.Integer
     * @author qian.zhou
     * @date 2021/8/29
     */
    Integer saveCourseComment(CourseCommentDTO dto);

    /**
     * 功能描述 根据课程id获取到对应的课程评论
     *
     * @param courseId
     * @param offset
     * @param pageSize
     * @return java.util.List<com.qianzhou.entity.CourseComment>
     * @author qian.zhou
     * @date 2021/8/30
     */

    List<CourseComment> getCommentsByCourseId(@Param("courseId") Integer courseId, @Param("offset") Integer offset, @Param("pageSize") Integer pageSize);

    /**
     * 功能描述 用户对这条留言是否点过赞
     *
     * @param commentId
     * @param userId
     * @return java.lang.Integer
     * @author qian.zhou
     * @date 2021/8/30
     */

    Integer existsFavorite(@Param("commentId") Integer commentId, @Param("userId") Integer userId);

    /**
     * 功能描述 保存点赞信息
     *
     * @param commentId
     * @param userId
     * @return java.lang.Integer
     * @author qian.zhou
     * @date 2021/8/30
     */

    Integer saveCommentFavorite(@Param("commentId") Integer commentId, @Param("userId") Integer userId);

    /**
     * 功能描述 更新点赞状态
     *
     * @param status
     * @param commentId
     * @param userId
     * @return java.lang.Integer
     * @author qian.zhou
     * @date 2021/8/30
     */

    Integer updateFavoriteStatus(@Param("status") Integer status, @Param("commentId") Integer commentId, @Param("userId") Integer userId);

    /**
     * 功能描述 更新点赞数量
     *
     * @param num
     * @param commentId
     * @return java.lang.Integer
     * @author qian.zhou
     * @date 2021/8/30
     */

    Integer updateLikeCount(@Param("num") Integer num, @Param("commentId") Integer commentId);
}

