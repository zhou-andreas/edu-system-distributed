package com.qianzhou.dao;

import com.qianzhou.entity.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 用户表(User)表数据库访问层
 *
 * @author qian.zhou
 * @since 2021-08-28 13:40:08
 */
@Mapper
public interface UserDao {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    User queryById(Integer id);

    /**
     * 查询指定行数据
     *
     * @param offset 查询起始位置
     * @param limit  查询条数
     * @return 对象列表
     */
    List<User> queryAllByLimit(@Param("offset") int offset, @Param("limit") int limit);


    /**
     * 通过实体作为筛选条件查询
     *
     * @param user 实例对象
     * @return 对象列表
     */
    List<User> queryAll(User user);

    /**
     * 新增数据
     *
     * @param user 实例对象
     * @return 影响行数
     */
    int insert(User user);

    /**
     * 批量新增数据（MyBatis原生foreach方法）
     *
     * @param entities List<User> 实例对象列表
     * @return 影响行数
     */
    int insertBatch(@Param("entities") List<User> entities);

    /**
     * 批量新增或按主键更新数据（MyBatis原生foreach方法）
     *
     * @param entities List<User> 实例对象列表
     * @return 影响行数
     */
    int insertOrUpdateBatch(@Param("entities") List<User> entities);

    /**
     * 修改数据
     *
     * @param user 实例对象
     * @return 影响行数
     */
    int update(User user);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 影响行数
     */
    int deleteById(Integer id);

    /**
     * 功能描述 用户登录
     *
     * @param phone
     * @param password
     * @return com.qianzhou.entity.User
     * @author qian.zhou
     * @date 2021/8/29
     */
    User Login(@Param("phone") String phone, @Param("password") String password);

    /**
     * 功能描述 检查手机号是否注册过
     *
     * @param phone
     * @return java.lang.Integer
     * @author qian.zhou
     * @date 2021/8/29
     */
    Integer checkPhone(String phone);

    /**
     * 功能描述 用户是否注册
     *
     * @param user
     * @return java.lang.Integer
     * @author qian.zhou
     * @date 2021/8/29
     */

    Integer register(User user);
}

