package com.qianzhou.dao;

import com.qianzhou.entity.UserCourseOrder;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 用户课程订单表(UserCourseOrder)表数据库访问层
 *
 * @author qian.zhou
 * @since 2021-08-28 13:40:09
 */
@Mapper
public interface UserCourseOrderDao {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    UserCourseOrder queryById(Long id);

    /**
     * 查询指定行数据
     *
     * @param offset 查询起始位置
     * @param limit  查询条数
     * @return 对象列表
     */
    List<UserCourseOrder> queryAllByLimit(@Param("offset") int offset, @Param("limit") int limit);


    /**
     * 通过实体作为筛选条件查询
     *
     * @param userCourseOrder 实例对象
     * @return 对象列表
     */
    List<UserCourseOrder> queryAll(UserCourseOrder userCourseOrder);

    /**
     * 新增数据
     *
     * @param userCourseOrder 实例对象
     * @return 影响行数
     */
    int insert(UserCourseOrder userCourseOrder);

    /**
     * 批量新增数据（MyBatis原生foreach方法）
     *
     * @param entities List<UserCourseOrder> 实例对象列表
     * @return 影响行数
     */
    int insertBatch(@Param("entities") List<UserCourseOrder> entities);

    /**
     * 批量新增或按主键更新数据（MyBatis原生foreach方法）
     *
     * @param entities List<UserCourseOrder> 实例对象列表
     * @return 影响行数
     */
    int insertOrUpdateBatch(@Param("entities") List<UserCourseOrder> entities);

    /**
     * 修改数据
     *
     * @param userCourseOrder 实例对象
     * @return 影响行数
     */
    int update(UserCourseOrder userCourseOrder);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 影响行数
     */
    int deleteById(Long id);

    /**
     * 功能描述
     *
     * @param userCourseOrder
     * @return void
     * @author qian.zhou
     * @date 2021/8/29
     */
    void savaOrder(UserCourseOrder userCourseOrder);

    /**
     * 功能描述 修改订单状态
     *
     * @param orderNo 订单号
     * @return void
     * @author qian.zhou
     * @date 2021/8/29
     */
    Integer updateOrder(Long orderNo, Byte status);

    /**
     * 功能描述 删除订单
     *
     * @param orderNo
     * @return void
     * @author qian.zhou
     * @date 2021/8/29
     */
    Integer deleteOrder(Long orderNo);
    /**
     *功能描述 查询已登录用户的全部订单
     * @author qian.zhou
     * @date 2021/8/29
     * @param userId
     * @return java.util.List<com.qianzhou.entity.UserCourseOrder>
     */

    List<UserCourseOrder> getOrdersByUserId(Integer userId);
}

