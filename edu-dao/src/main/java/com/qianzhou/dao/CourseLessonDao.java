package com.qianzhou.dao;

import com.qianzhou.entity.CourseLesson;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 课程节内容(CourseLesson)表数据库访问层
 *
 * @author qian.zhou
 * @since 2021-08-28 13:40:04
 */
@Mapper
public interface CourseLessonDao {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    CourseLesson queryById(Object id);

    /**
     * 查询指定行数据
     *
     * @param offset 查询起始位置
     * @param limit  查询条数
     * @return 对象列表
     */
    List<CourseLesson> queryAllByLimit(@Param("offset") int offset, @Param("limit") int limit);


    /**
     * 通过实体作为筛选条件查询
     *
     * @param courseLesson 实例对象
     * @return 对象列表
     */
    List<CourseLesson> queryAll(CourseLesson courseLesson);

    /**
     * 新增数据
     *
     * @param courseLesson 实例对象
     * @return 影响行数
     */
    int insert(CourseLesson courseLesson);

    /**
     * 批量新增数据（MyBatis原生foreach方法）
     *
     * @param entities List<CourseLesson> 实例对象列表
     * @return 影响行数
     */
    int insertBatch(@Param("entities") List<CourseLesson> entities);

    /**
     * 批量新增或按主键更新数据（MyBatis原生foreach方法）
     *
     * @param entities List<CourseLesson> 实例对象列表
     * @return 影响行数
     */
    int insertOrUpdateBatch(@Param("entities") List<CourseLesson> entities);

    /**
     * 修改数据
     *
     * @param courseLesson 实例对象
     * @return 影响行数
     */
    int update(CourseLesson courseLesson);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 影响行数
     */
    int deleteById(Object id);

}

