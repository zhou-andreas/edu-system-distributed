package com.qianzhou.dao;

import com.qianzhou.bo.CourseBO;
import com.qianzhou.entity.Course;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 课程(Course)表数据库访问层
 *
 * @author qian.zhou
 * @since 2021-08-28 13:40:03
 */
@Mapper
public interface CourseDao {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    Course queryById(Object id);

    /**
     * 查询指定行数据
     *
     * @param offset 查询起始位置
     * @param limit  查询条数
     * @return 对象列表
     */
    List<Course> queryAllByLimit(@Param("offset") int offset, @Param("limit") int limit);


    /**
     * 通过实体作为筛选条件查询
     *
     * @param course 实例对象
     * @return 对象列表
     */
    List<Course> queryAll(Course course);

    /**
     * 新增数据
     *
     * @param course 实例对象
     * @return 影响行数
     */
    int insert(Course course);

    /**
     * 批量新增数据（MyBatis原生foreach方法）
     *
     * @param entities List<Course> 实例对象列表
     * @return 影响行数
     */
    int insertBatch(@Param("entities") List<Course> entities);

    /**
     * 批量新增或按主键更新数据（MyBatis原生foreach方法）
     *
     * @param entities List<Course> 实例对象列表
     * @return 影响行数
     */
    int insertOrUpdateBatch(@Param("entities") List<Course> entities);

    /**
     * 修改数据
     *
     * @param course 实例对象
     * @return 影响行数
     */
    int update(Course course);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 影响行数
     */
    int deleteById(Object id);

    /**
     * @Author: andreaszhou
     * @Description: 全部课程
     * @DateTime: 2021/8/28 20:27
     * @Params:
     * @Return
     */
    List<CourseBO> getAllCourse();

    /**
     * @Author: andreaszhou
     * @Description: 查看已购课程
     * @DateTime: 2021/8/29 9:13
     * @Params: id
     * @Return
     */
    List<CourseBO> getCourseByUserId(Integer id);

    /**
     * 功能描述 根据课程id查询相应的课程信息
     *
     * @param courseId
     * @return com.qianzhou.bo.CourseBO
     * @author qian.zhou
     * @date 2021/8/29
     */

    CourseBO getCourseById(Integer courseId);
}

