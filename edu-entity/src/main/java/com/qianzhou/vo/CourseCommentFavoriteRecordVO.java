package com.qianzhou.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;
import java.util.Date;

/**
 * 课程留言点赞表(CourseCommentFavoriteRecord)实体类
 *
 * @author qian.zhou
 * @since 2021-08-28 13:40:04
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class CourseCommentFavoriteRecordVO implements Serializable {
    private static final long serialVersionUID = 718735046613270228L;
    /**
     * 用户评论点赞j记录ID
     */
    private Integer id;
    /**
     * 用户ID
     */
    private Integer userId;
    /**
     * 用户评论ID
     */
    private Integer commentId;
    /**
     * 是否删除，0：未删除（已赞），1：已删除（取消赞状态）
     */
    private Object isDel;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 更新时间
     */
    private Date updateTime;
}
