package com.qianzhou.vo;

import com.qianzhou.bo.CourseMediaBO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;
import java.util.Date;

/**
 * 课程节内容(CourseLesson)实体类
 *
 * @author qian.zhou
 * @since 2021-08-28 13:40:04
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class CourseLessonVO implements Serializable {
    private static final long serialVersionUID = -30472928410944783L;
    /**
     * id
     */
    private Object id;
    /**
     * 课程id
     */
    private Integer courseId;
    /**
     * 章节id
     */
    private Integer sectionId;
    /**
     * 课时主题
     */
    private String theme;
    /**
     * 课时时长(分钟)
     */
    private Integer duration;
    /**
     * 是否免费
     */
    private Object isFree;
    /**
     * 记录创建时间
     */
    private Date createTime;
    /**
     * 更新时间
     */
    private Date updateTime;
    /**
     * 是否删除
     */
    private Object isDel;
    /**
     * 排序字段
     */
    private Integer orderNum;
    /**
     * 课时状态,0-隐藏，1-未发布，2-已发布
     */
    private Integer status;
    /**
     * 视频
     */
    private CourseMediaVO courseMedia; // 一小节课对应一个视频
}
