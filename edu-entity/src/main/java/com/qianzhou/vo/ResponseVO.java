package com.qianzhou.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;

/**
 * 描述：数据传输对象，返回api接口格式
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class ResponseVO<T> implements Serializable {
    private static final long serialVersionUID = 1L;
    private int state; // 操作状态
    private String message; // 状态描述
    private T content; // 相应内容
}
