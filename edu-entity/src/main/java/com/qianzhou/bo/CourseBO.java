package com.qianzhou.bo;


import com.qianzhou.vo.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.springframework.beans.BeanUtils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 课程(Course)实体类
 *
 * @author qian.zhou
 * @since 2021-08-28 13:40:03
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class CourseBO implements Serializable {
    private static final long serialVersionUID = -88505329377053816L;
    /**
     * id
     */
    private Object id;
    /**
     * 课程名
     */
    private String courseName;
    /**
     * 课程一句话简介
     */
    private String brief;
    /**
     * 原价
     */
    private Object price;
    /**
     * 原价标签
     */
    private String priceTag;
    /**
     * 优惠价
     */
    private Object discounts;
    /**
     * 优惠标签
     */
    private String discountsTag;
    /**
     * 描述markdown
     */
    private Object courseDescriptionMarkDown;
    /**
     * 课程描述
     */
    private Object courseDescription;
    /**
     * 课程分享图片url
     */
    private String courseImgUrl;
    /**
     * 是否新品
     */
    private Object isNew;
    /**
     * 广告语
     */
    private String isNewDes;
    /**
     * 最后操作者
     */
    private Integer lastOperatorId;
    /**
     * 自动上架时间
     */
    private Date autoOnlineTime;
    /**
     * 记录创建时间
     */
    private Date createTime;
    /**
     * 更新时间
     */
    private Date updateTime;
    /**
     * 是否删除
     */
    private Object isDel;
    /**
     * 总时长(分钟)
     */
    private Integer totalDuration;
    /**
     * 课程列表展示图片
     */
    private String courseListImg;
    /**
     * 课程状态，0-草稿，1-上架
     */
    private Integer status;
    /**
     * 课程排序，用于后台保存草稿时用到
     */
    private Integer sortNum;
    /**
     * 课程预览第一个字段
     */
    private String previewFirstField;
    /**
     * 课程预览第二个字段
     */
    private String previewSecondField;
    /**
     * 销量
     */
    private Integer sales;
    /**
     * 讲师
     */
    private TeacherBO teacher;
    /**
     * 课程章节列表
     */
    private List<CourseSectionBO> courseSections;
    /**
     * 课程活动
     */
    private ActivityCourseBO activityCourse;

    /**
     * 将CourseBO转换为CourseVO
     */
    public static CourseVO boConvertVo(CourseBO bo) {
        CourseVO vo = new CourseVO();
        BeanUtils.copyProperties(bo, vo);
        TeacherBO teacherBO = bo.getTeacher();
        if (teacherBO == null) {
            vo.setTeacher(null);
        } else {
            TeacherVO teacherVO = new TeacherVO();
            BeanUtils.copyProperties(teacherBO, teacherVO);
            vo.setTeacher(teacherVO);
        }
        List<CourseSectionBO> courseSectionBOS = bo.getCourseSections();
        List<CourseSectionVO> courseSectionVOS = new ArrayList<>();
        for (CourseSectionBO courseSectionBO : courseSectionBOS
        ) {
            if (courseSectionBO == null) {
                courseSectionVOS.add(null);
            } else {
                CourseSectionVO courseSectionVO = new CourseSectionVO();
                BeanUtils.copyProperties(courseSectionBO, courseSectionVO);
                List<CourseLessonBO> courseLessonBOS = courseSectionBO.getCourseLessons();
                List<CourseLessonVO> courseLessonVOS = new ArrayList<>();
                for (CourseLessonBO courseLessonBO: courseLessonBOS
                     ) {
                    if (courseLessonBO == null){
                        courseLessonVOS.add(null);
                    }else {
                        CourseLessonVO courseLessonVO = new CourseLessonVO();
                        CourseMediaBO courseMediaBO = courseLessonBO.getCourseMedia();
                        if (courseMediaBO==null){
                            courseLessonVO.setCourseMedia(null);
                        }else {
                            CourseMediaVO courseMediaVO = new CourseMediaVO();
                            BeanUtils.copyProperties(courseMediaBO,courseMediaVO);
                            courseLessonVO.setCourseMedia(courseMediaVO);
                        }
                        BeanUtils.copyProperties(courseLessonBO,courseLessonVO);
                        courseLessonVOS.add(courseLessonVO);
                    }
                }
                courseSectionVO.setCourseLessons(courseLessonVOS);
                courseSectionVOS.add(courseSectionVO);
            }
        }
        vo.setCourseSections(courseSectionVOS);
        ActivityCourseBO activityCourseBO = bo.getActivityCourse();
        if (activityCourseBO == null) {
            vo.setActivityCourse(null);
        } else {
            ActivityCourseVO activityCourseVO = new ActivityCourseVO();
            BeanUtils.copyProperties(activityCourseBO, activityCourseVO);
            vo.setActivityCourse(activityCourseVO);
        }
        return vo;
    }

    /**
     * 将List<CourseBO>转换为List<CourseVO>
     */
    public static List<CourseVO> boListConvertVoList(List<CourseBO> courseBOS) {
        List<CourseVO> courseVOS = new ArrayList<>();
        for (CourseBO bo : courseBOS
        ) {
            CourseVO courseVO = CourseBO.boConvertVo(bo);
            courseVOS.add(courseVO);
        }
        return courseVOS;
    }
}
