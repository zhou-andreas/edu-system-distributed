package com.qianzhou.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;
import java.util.Date;

/**
 * (CoursePlayHistory)实体类
 *
 * @author qian.zhou
 * @since 2021-08-28 13:40:05
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class CoursePlayHistory implements Serializable {
    private static final long serialVersionUID = 650600669682397097L;
    /**
     * id
     */
    private Object id;
    /**
     * 用户id
     */
    private Integer userId;
    /**
     * 课程id
     */
    private Integer courseId;
    /**
     * 章节id
     */
    private Integer sectionId;
    /**
     * 课时id
     */
    private Integer lessonId;
    /**
     * 历史播放节点(s)
     */
    private Integer historyNode;
    /**
     * 最高历史播放节点
     */
    private Integer historyHighestNode;
    /**
     * 记录创建时间
     */
    private Date createTime;
    /**
     * 更新时间
     */
    private Date updateTime;
    /**
     * 是否删除
     */
    private Object isDel;
}
