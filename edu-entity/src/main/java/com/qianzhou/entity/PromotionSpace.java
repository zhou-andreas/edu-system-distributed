package com.qianzhou.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;
import java.util.Date;

/**
 * 广告区域表(PromotionSpace)实体类
 *
 * @author qian.zhou
 * @since 2021-08-28 13:40:06
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class PromotionSpace implements Serializable {
    private static final long serialVersionUID = 426973835988053155L;

    private Integer id;
    /**
     * 名称
     */
    private String name;
    /**
     * 广告位key
     */
    private String spacekey;

    private Date createtime;

    private Date updatetime;

    private Integer isdel;
}
