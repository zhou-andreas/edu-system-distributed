package com.qianzhou.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;
import java.util.Date;

/**
 * 支付订单状态日志表(PayOrderRecord)实体类
 *
 * @author qian.zhou
 * @since 2021-08-28 13:40:06
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class PayOrderRecord implements Serializable {
    private static final long serialVersionUID = 432764504960524176L;
    /**
     * ID
     */
    private Integer id;
    /**
     * 订单号
     */
    private String orderNo;
    /**
     * 操作类型：CREATE|PAY|REFUND...
     */
    private String type;
    /**
     * 原订单状态
     */
    private String fromStatus;
    /**
     * 新订单状态
     */
    private String toStatus;
    /**
     * 实付金额，单位为分
     */
    private Integer paidAmount;
    /**
     * 备注
     */
    private String remark;
    /**
     * 操作人
     */
    private String createdBy;
    /**
     * 操作时间
     */
    private Date createdAt;
}
