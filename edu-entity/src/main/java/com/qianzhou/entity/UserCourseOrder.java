package com.qianzhou.entity;

import com.qianzhou.vo.UserCourseOrderVO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.springframework.beans.BeanUtils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 用户课程订单表(UserCourseOrder)实体类
 *
 * @author qian.zhou
 * @since 2021-08-28 13:40:09
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class UserCourseOrder implements Serializable {
    private static final long serialVersionUID = -22801014577173591L;
    /**
     * 主键
     */
    private Long id;
    /**
     * 订单号
     */
    private String orderNo;
    /**
     * 用户id
     */
    private Object userId;
    /**
     * 课程id，根据订单中的课程类型来选择
     */
    private Object courseId;
    /**
     * 活动课程id
     */
    private Integer activityCourseId;
    /**
     * 订单来源类型: 1 用户下单购买 2 后台添加专栏
     */
    private Object sourceType;
    /**
     * 当前状态: 0已创建 10未支付 20已支付 30已取消 40已过期
     */
    private Object status;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 更新时间
     */
    private Date updateTime;
    /**
     * 是否删除
     */
    private Object isDel;

    /**
     * 功能描述 将entity转换为vo
     *
     * @param userCourseOrder
     * @return com.qianzhou.vo.UserCourseOrderVO
     * @author qian.zhou
     * @date 2021/8/29
     */

    public static UserCourseOrderVO poConvertVo(UserCourseOrder userCourseOrder) {
        UserCourseOrderVO userCourseOrderVO = new UserCourseOrderVO();
        BeanUtils.copyProperties(userCourseOrder, userCourseOrderVO);
        return userCourseOrderVO;
    }

    public static List<UserCourseOrderVO> poListConvertVoList(List<UserCourseOrder> userCourseOrders) {
        List<UserCourseOrderVO> userCourseOrderVOS = new ArrayList<>();
        for (UserCourseOrder userCourseOrder : userCourseOrders
        ) {
            UserCourseOrderVO userCourseOrderVO = poConvertVo(userCourseOrder);
            userCourseOrderVOS.add(userCourseOrderVO);
        }
        return userCourseOrderVOS;
    }
}
