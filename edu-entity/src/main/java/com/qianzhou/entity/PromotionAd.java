package com.qianzhou.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;
import java.util.Date;

/**
 * 广告表(PromotionAd)实体类
 *
 * @author qian.zhou
 * @since 2021-08-28 13:40:06
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class PromotionAd implements Serializable {
    private static final long serialVersionUID = -34168170544061862L;

    private Integer id;
    /**
     * 广告名
     */
    private String name;
    /**
     * 广告位id
     */
    private Integer spaceid;
    /**
     * 精确搜索关键词
     */
    private String keyword;
    /**
     * 静态广告的内容
     */
    private String htmlcontent;
    /**
     * 文字一
     */
    private String text;
    /**
     * 链接一
     */
    private String link;
    /**
     * 开始时间
     */
    private Date starttime;
    /**
     * 结束时间
     */
    private Date endtime;

    private Date createtime;

    private Date updatetime;

    private Integer status;
    /**
     * 优先级
     */
    private Integer priority;

    private String img;
}
